#include <stdbool.h>
#include <stdint.h>
#include "nrfx_gpiote.h"
#include "nrfx_uarte.h"
#include "nrf_delay.h"

#define LED_COUNT 4

static void initGpio(void);
static void switchLeds(void);
static void initUart(void);
static void sendUart(void);

static const uint32_t RxPin = 8;
static const uint32_t TxPin = 6;
static const nrfx_uarte_t uarte = NRFX_UARTE_INSTANCE(0);
static const uint32_t ledPins[LED_COUNT] = {17,18,19,20};
static uint32_t idx = 0;

int main(void)
{
    initGpio();
    initUart();
    while (true)
    {
        switchLeds();
        sendUart();
        nrf_delay_ms(2000);
    }
}

static void initGpio(void)
{
    nrfx_gpiote_init();
    for (uint32_t i = 0; i < LED_COUNT; i++) {
        const nrfx_gpiote_out_config_t cfg = {
            .action = NRF_GPIOTE_POLARITY_LOTOHI,
            .init_state = NRF_GPIOTE_INITIAL_VALUE_HIGH,
            .task_pin = false
        };
        nrfx_gpiote_out_init((nrfx_gpiote_pin_t)ledPins[i], &cfg);
    }
}

static void switchLeds(void)
{
    for (uint32_t i = 0; i < LED_COUNT; i++) {
        if (idx == i) {
            nrfx_gpiote_out_clear((nrfx_gpiote_pin_t)ledPins[i]);
        } else {
            nrfx_gpiote_out_set((nrfx_gpiote_pin_t)ledPins[i]);
        }
    }
    idx++;
    if (idx >= LED_COUNT) {
        idx = 0;
    }
}

static void initUart(void)
{
    static const nrfx_uarte_config_t config = {
        .pseltxd = TxPin,
        .pselrxd = RxPin,
        .pselcts = NRF_UARTE_PSEL_DISCONNECTED,
        .pselrts = NRF_UARTE_PSEL_DISCONNECTED,
        .p_context          = NULL,
        .hwfc               = NRF_UARTE_HWFC_DISABLED,
        .parity             = NRF_UARTE_PARITY_EXCLUDED,
        .baudrate           = NRF_UARTE_BAUDRATE_9600,
        .interrupt_priority = NRFX_UARTE_DEFAULT_CONFIG_IRQ_PRIORITY,
    };
    nrfx_err_t err = nrfx_uarte_init(&uarte, &config, NULL);
    if (err != NRF_SUCCESS) {
        return;
    }
    static const uint8_t data[] = "Hello, starting\r\n";
    err = nrfx_uarte_tx(&uarte, data, sizeof(data));
}

static void sendUart(void)
{
    char data[] = "Led idx: n\r\n";
    switch (idx) {
    case 0:
        data[9] = '0';
        break;
    case 1:
        data[9] = '1';
        break;
    case 2:
        data[9] = '2';
        break;
    case 3:
        data[9] = '3';
        break;
    default:
        break;
    }
    nrfx_err_t err = nrfx_uarte_tx(&uarte, data, sizeof(data));
}
